const express = require("express");
const app = express();
const port = process.env.PORT || 3000

app.use(express.static(__dirname + '/views'));

app.set('view engine', 'pug');

app.listen(port, () => console.log(`Escuchando tus peticiones en el puerto ${port}`));